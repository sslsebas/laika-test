import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../interfaces/pokemon.interface';

@Injectable({
  providedIn: 'root',
})
export class PokeService {
  private _serviceURL: string = 'https://pokeapi.co/api/v2/pokemon/';
  private _response!: Pokemon;
  private _myPokemons: Pokemon[] = [];

  get myPokemons(): Pokemon[] {
    return [...this._myPokemons];
  }

  constructor(private http: HttpClient) {}

  /**
   * @description Get one Pokemon!
   * @param idPokemon Between 1 to 898
   */
  searchPokemon(idPokemon: number): Promise<Pokemon> {
    return this.http
      .get<Pokemon>(`${this._serviceURL}${idPokemon}`)
      .toPromise();
  }

  /**
   *
   * @returns a random number between 1 - 898
   */
  randomNumber(): number {
    let rand = Math.floor(Math.random() * 898);
    return rand;
  }

  /**
   * @description push 6 *new* Pokemons on public myPokemons
   */
  async createMyPokemons() {
    this._myPokemons = [];
    for (let index = 0; index < 8; index++) {
      let newPokemon: any = await this.searchPokemon(this.randomNumber());
      this._myPokemons.push(newPokemon);
    }
  }
}
