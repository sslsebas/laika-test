import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [
    './navbar.component.css',
    './header.component.css',
    './dnavbar.component.css',
  ],
})
export class NavbarComponent implements OnInit {
  categories: { name: string; icon: string }[] = [
    { name: 'Alimento', icon: 'food' },
    { name: 'Snacks', icon: 'bone' },
    { name: 'Farmapet', icon: 'health' },
    { name: 'Cuidado e Higiene', icon: 'shampoo' },
    { name: 'Juguetes', icon: 'ball' },
    { name: 'Accesorios', icon: 'necklace' },
    { name: 'Para PetLovers', icon: 'owner' },
  ];

  constructor() {}

  ngOnInit(): void {}
}
