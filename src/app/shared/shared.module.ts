import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { SuggestionsComponent } from './suggestions/suggestions.component';

@NgModule({
  declarations: [
    NavbarComponent,
    CarouselComponent,
    FooterComponent,
    ContactUsComponent,
    SuggestionsComponent,
  ],
  exports: [
    NavbarComponent,
    CarouselComponent,
    FooterComponent,
    ContactUsComponent,
    SuggestionsComponent,
  ],
  imports: [CommonModule],
})
export class SharedModule {}
