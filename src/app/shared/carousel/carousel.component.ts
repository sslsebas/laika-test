import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css'],
})
export class CarouselComponent implements OnInit {
  moving: any = null;
  listImgs: string[];
  selectedImg: number = 0;

  constructor() {
    this.listImgs = ['1', '2', '3', '4', '5'];
  }

  ngOnInit(): void {
    this.moving = setInterval(() => {
      this.nextImg();
    }, 5000);
  }

  ngOnDestroy() {
    if (this.moving) {
      clearInterval(this.moving);
    }
  }

  nextImg() {
    if (this.selectedImg + 2 > this.listImgs.length) {
      this.selectedImg = 0;
    } else {
      this.selectedImg += 1;
    }
  }

  selectImg(imgIndex: number) {
    this.selectedImg = imgIndex;
  }
}
