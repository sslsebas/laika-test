import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Pokemon } from 'src/app/interfaces/pokemon.interface';
import { PokeService } from 'src/app/services/poke.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: [
    './main.component.css',
    './card.component.css',
    './bottom.component.css',
  ],
})
export class MainComponent implements OnInit {
  leftOff: number = 0;
  rightOff: number = 0;
  maxScrollLeft: number = 0;
  @ViewChild('brandContainer') brandContainer!: ElementRef;

  get randomPokemons(): Pokemon[] {
    return this.pokeService.myPokemons;
  }

  categories: { name: string; icon: string }[] = [
    { name: 'Alimento', icon: 'food' },
    { name: 'Snacks', icon: 'bone' },
    { name: 'Farmapet', icon: 'health' },
    { name: 'Cuidado e Higiene', icon: 'shampoo' },
    { name: 'Juguetes', icon: 'ball' },
    { name: 'Accesorios', icon: 'necklace' },
    { name: 'Para PetLovers', icon: 'owner' },
  ];
  brands: string[] = [
    'ABACEL.png',
    'Acana.png',
    'ADAPTIL.png',
    'Advance.png',
    'Advance2.jpg',
    'advantage.png',
    'advantix.png',
    'notFound.jpeg',
  ];

  constructor(private pokeService: PokeService) {}

  ngOnInit(): void {
    this.pokeService.createMyPokemons();
  }

  scrollCheck() {
    this.maxScrollLeft =
      this.brandContainer.nativeElement.scrollWidth -
      this.brandContainer.nativeElement.offsetWidth;
    this.leftOff = this.brandContainer.nativeElement.scrollLeft;
    this.rightOff = -this.brandContainer.nativeElement.scrollLeft;
  }

  scrollRight() {
    this.brandContainer.nativeElement.scrollLeft +=
      (this.brandContainer.nativeElement.scrollWidth -
        this.brandContainer.nativeElement.offsetWidth) *
      0.4;
  }

  scrollLeft() {
    this.brandContainer.nativeElement.scrollLeft -=
      (this.brandContainer.nativeElement.scrollWidth -
        this.brandContainer.nativeElement.offsetWidth) *
      0.4;
  }

  createNewPokemons() {
    this.pokeService.createMyPokemons();
  }
}
